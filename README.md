<div id="top"></div>

<br />
<div align="center">
  <a href="https://gitlab.com/doshinxlsx/starwars-wiki">
    <img src="https://i.imgur.com/GbLq58e.png" alt="SWWiki logo" width="80" height="80">
  </a>

<h3 align="center">Starwars Wikipedia</h3>
</div>

## About The Project

![Starwars Wikipedia](https://i.imgur.com/8wEpBYm.png)

Simple app for Star Wars trilogy fanatics. It contains data from SEVEN Star Wars movies.

### Built With

* [React.js](https://reactjs.org/)

## Usage

Search for your favourite characters or browse the species of the Galactic Empire.

## Contact

Dominik Orincsai - o.dominik2107@gmail.com
[LinkedIn](https://www.linkedin.com/in/dominikorincsai/)

Project Link: [https://gitlab.com/doshinxlsx/starwars-wiki](https://gitlab.com/doshinxlsx/starwars-wiki)

[product-screenshot]: https://imgur.com/a/dpDxxYb