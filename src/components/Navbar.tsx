import React, { useEffect, useState } from "react";
import {
  Box,
  Flex,
  Icon,
  Img,
  Input,
  InputGroup,
  InputRightElement,
  VStack,
} from "@chakra-ui/react";
import { Link } from "react-router-dom";
import { FaSearch } from "react-icons/fa";

import useFetch from "../hooks/useFetch";
import Results from "./Results";

const Navbar = () => {
  const [showResults, setShowResults] = useState(false);
  const { data, setData } = useFetch();

  useEffect(() => {
    if (data.slug.length === 0) {
      setShowResults(false);
    } else {
      setShowResults(true);
    }
  }, [data.slug]);

  return (
    <Flex
      w="95%"
      h="65px"
      bgColor="black"
      borderBottomLeftRadius="6px"
      borderBottomRightRadius="6px"
      boxShadow="dark-lg"
      marginLeft="auto"
      marginRight="auto"
    >
      {/* Logo */}
      <Link to="/">
        <Box
          _focus={{ outline: "none" }}
          zIndex="9999"
          position="relative"
          left={3}
          top={-10}
        >
          <Img src="/images/logo.png" w="160px" h="160px" />
        </Box>
      </Link>

      {/* Search input */}
      <Box position="absolute" right={16} top={3} w="240px">
        <VStack spacing={2}>
          <Input
            type="text"
            placeholder="Search characters"
            value={data.slug}
            onChange={(e) => setData({ ...data, slug: e.target.value })}
            _focus={{ outline: "none" }}
          />
          {data.results?.results?.length > 0
            ? showResults && <Results result={data.results.results} />
            : null}
        </VStack>
      </Box>
    </Flex>
  );
};

export default Navbar;
