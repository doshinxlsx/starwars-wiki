import { Flex, Text } from "@chakra-ui/react";
import React from "react";

const NotFound = () => {
  return (
    <Flex w='100%' h='100vh' justifyContent="center" alignItems="center">
      <Text color='white'>404 Page not found</Text>
    </Flex>
  );
};

export default NotFound;
