import { Box, Flex, Text } from "@chakra-ui/react";
import { Link, useNavigate } from "react-router-dom";

const Results = ({ result }: any) => {
  const navigate = useNavigate();

  return (
    <Flex
      w="240px"
      h="fit-content"
      bgColor="rgba(0, 0, 0, 0.6)"
      zIndex="999"
      borderRadius="6px"
    >
      <Box w="100%">
        {result.map((person: any, i: any) => {
          return (
            <>
              <Box
                _hover={{
                  cursor: "pointer",
                  backgroundColor: "rgba(255, 255, 255, 0.08)",
                }}
                onClick={() =>
                    navigate(`/profile/${person.name}`, {
                    state: {
                      result: result,
                      films: person.films,
                      species: person.species,
                    },
                  })
                }
              >
                <Text key={`person-id-${i}`} padding={4}>
                  {person.name}
                </Text>
              </Box>
            </>
          );
        })}
      </Box>
    </Flex>
  );
};

export default Results;
