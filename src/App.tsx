import React, { FC, useEffect, useState } from "react";
import { ChakraProvider } from "@chakra-ui/react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import "./App.css";

// Containers
import Main from "./containers/main";
import Home from "./containers/home";
import Race from "./containers/race";
import Profile from "./containers/profile";

// Components
import NotFound from "./components/NotFound";

const App = () => {
  return (
    <ChakraProvider>
      <Router>
        <Main>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/races/:race" element={<Race />} />
            <Route path="/profile/:name" element={<Profile />} />
            <Route path="*" element={<NotFound />} />
          </Routes>
        </Main>
      </Router>
    </ChakraProvider>
  );
};

export default App;
