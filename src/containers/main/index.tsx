import { Box, Flex, Text } from "@chakra-ui/react";
import React from "react";
import Navbar from "../../components/Navbar";

const Main = ({ children }: any) => {
  return (
    <Flex w="100%" h="100vh" bgColor="whitesmoke" justifyContent="center">
      {/* Nav */}
      <Navbar />

      {/* Container */}
      <Flex
        w="95%"
        h="91.5vh"
        bgColor="black"
        position="fixed"
        bottom={0}
        borderRadius="6px"
        boxShadow="dark-lg"
      >
        {children}
      </Flex>
    </Flex>
  );
};

export default Main;
