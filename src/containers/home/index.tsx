import {
  Box,
  chakra,
  Flex,
  Grid,
  GridItem,
  Img,
  SimpleGrid,
  Text,
  useColorModeValue,
} from "@chakra-ui/react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import React, { useEffect, useState } from "react";
import axios from "axios";
import PulseLoader from "react-spinners/PulseLoader";

let raceBG = new Map<string, string>([
  ["Human", "/images/species/Human.jpeg"],
  ["Droid", "/images/species/Droid.png"],
  ["Wookie", "/images/species/Wookie.png"],
  ["Rodian", "/images/species/Rodian.jpeg"],
  ["Hutt", "/images/species/Hutt.png"],
  ["Yoda's species", "/images/species/Yoda.png"],
  ["Trandoshan", "/images/species/Trandoshan.png"],
  ["Mon Calamari", "/images/species/Calamari.png"],
  ["Ewok", "/images/species/Ewok.png"],
  ["Sullustan", "/images/species/Sullustan.png"],
]);

const Home = () => {
  const [allSpecies, setAllSpecies] = useState<any>({
    species: [],
    isLoading: true,
  });

  useEffect(() => {
    fetch("https://swapi.dev/api/species/")
      .then((res) => res.json())
      .then((json) => {
        setAllSpecies({
          species: json,
          isLoading: false,
        });
      });
  }, []);

  const { isLoading, species } = allSpecies;
  const { results } = species;

  const navigate = useNavigate();

  return (
    <>
      {isLoading ? (
        <Flex w="100%" justifyContent="center" alignItems="center">
          <Text color="white" fontWeight="600" fontSize="16px">
            <PulseLoader color="white" />
          </Text>
        </Flex>
      ) : (
        <Flex
          w="100%"
          position="relative"
          overflowY="scroll"
          scrollBehavior="smooth"
          justifyContent="center"
          alignItems="center"
          p={6}
        >
          <Box>
            <Text
              fontWeight="600"
              position="absolute"
              top={4}
              left={4}
              fontSize="36px"
            >
              Species
            </Text>
          </Box>
          <SimpleGrid
            position="absolute"
            top={0}
            columns={[1, 1, 2, 3, 4, 5]}
            spacing="32px"
            mt={8}
          >
            {results.map((result: any, i: any) => {
              return (
                <Flex
                  bg="transparent"
                  p={50}
                  w="full"
                  alignItems="center"
                  justifyContent="center"
                >
                  <Flex
                    key={`race-card-${i}`}
                    direction="column"
                    justifyContent="center"
                    alignItems="center"
                    w="sm"
                    mx="auto"
                    _hover={{ cursor: "pointer" }}
                    onClick={() =>
                      navigate(`/races/${result.name}`, {
                        state: { people: result.people, race: result.name },
                      })
                    }
                  >
                    <Box
                      bg="blackAlpha.400"
                      h={64}
                      w="full"
                      rounded="lg"
                      shadow="md"
                      bgSize="contain"
                      bgRepeat="no-repeat"
                      bgPos="center"
                      bgImg={raceBG.get(result.name)}
                    />

                    <Box
                      w={{ base: 56, md: 64 }}
                      bg="rgba(255, 255, 255, 0.03)"
                      mt={-10}
                      shadow="lg"
                      rounded="lg"
                      overflow="hidden"
                      boxShadow="0 4px 30px rgba(0, 0, 0, 0.1)"
                      backdropFilter="blur(6px)"
                    >
                      <chakra.h3
                        py={2}
                        textAlign="center"
                        fontWeight="bold"
                        textTransform="uppercase"
                        color={"white"}
                        letterSpacing={1}
                      >
                        {result.name}
                      </chakra.h3>
                    </Box>
                  </Flex>
                </Flex>
              );
            })}
          </SimpleGrid>
        </Flex>
      )}
    </>
  );
};

export default Home;
