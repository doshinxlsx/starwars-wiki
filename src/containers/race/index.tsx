import { Box, Flex, Text, VStack } from "@chakra-ui/react";
import axios from "axios";
import React, { useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { PulseLoader } from "react-spinners";

const Race = () => {
  const { state } = useLocation();
  const { result, people, race }: any = state || {};
  const navigate = useNavigate();

  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [isCharactersLoading, setIsCharactersLoading] = useState<boolean>(true);

  const [data, setData] = useState<any[]>([]);

  useEffect(() => {
    const fetchPeople = async () => {
      return await Promise.all(
        people.map((person: any) => {
          return fetch(person);
        })
      );
    };

    fetchPeople()
      .then((results) => Promise.all(results.map((r) => r.json())))
      .then((r) => setData(r))
      .then(() => setIsCharactersLoading(false));
    setIsLoading(false);
  }, []);

  return (
    <>
      {isLoading ? (
        <Text color="white">
          <PulseLoader color="white" />
        </Text>
      ) : (
        <Flex w="100%" justifyContent="center" alignItems="center">
          <Box>
            <Text
              fontWeight="600"
              position="absolute"
              top={4}
              left={4}
              fontSize="36px"
            >
              Details of {race}
            </Text>
          </Box>

          <Flex
            w="350px"
            h="250px"
            borderRadius="6px"
            justifyContent="center"
            bgColor="rgba(35, 35, 35, 0.6)"
            style={{
              boxShadow: "rgba(255, 255, 255, 0.3) 0px 0px 0px 3px",
            }}
          >
            <VStack>
              <Text fontSize="36px" fontWeight="600" textDecoration="underline">
                Characters
              </Text>

              {data.map((character: any, i: any) => {
                return (
                  <>
                    {isCharactersLoading ? (
                      <Text color="white">
                        <PulseLoader color="white" />
                      </Text>
                    ) : (
                      <Text
                        key={`character-id-${i}`}
                        fontWeight="500"
                      >
                        {character.name}
                      </Text>
                    )}
                  </>
                );
              })}
            </VStack>
          </Flex>
        </Flex>
      )}
    </>
  );
};

export default Race;
