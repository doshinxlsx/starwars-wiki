import { Box, Flex, HStack, Text, VStack } from "@chakra-ui/react";
import axios from "axios";
import React, { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import { PulseLoader } from "react-spinners";

const Profile = () => {
  const { state } = useLocation();
  const { result, films, species }: any = state || {};

  const [filmData, setFilmData] = useState<any[]>([]);
  const [speciesData, setSpeciesData] = useState<any[]>([]);

  const [isLoading, setIsLoading] = useState<boolean>(false);

  useEffect(() => {
    const fetchFilms = async () => {
      return await Promise.all(
        films.map((film: any) => {
          return fetch(film);
        })
      );
    };

    const fetchSpecies = async () => {
      return await Promise.all(
        species.map((data: any) => {
          return fetch(data);
        })
      );
    };

    fetchFilms()
      .then((results) => Promise.all(results.map((r) => r.json())))
      .then((r) => setFilmData(r));

    fetchSpecies()
      .then((results) => Promise.all(results.map((r) => r.json())))
      .then((r) => setSpeciesData(r));

    setIsLoading(false);
  }, []);

  return (
    <>
      {isLoading ? (
        <Text color="white">
          <PulseLoader color="white" />
        </Text>
      ) : (
        <Flex w="100%" justifyContent="center" alignItems="center">
          <Box>
            <Text
              fontWeight="600"
              position="absolute"
              top={4}
              left={4}
              fontSize="36px"
            >
              Profile
            </Text>
          </Box>

          <Flex
            w="80%"
            h="65%"
            borderRadius="6px"
            justifyContent="center"
            alignItems="center"
            bgColor="rgba(35, 35, 35, 0.6)"
            style={{
              boxShadow: "rgba(255, 255, 255, 0.3) 0px 0px 0px 3px",
            }}
          >
            {result.map((details: any, i: any) => {
              return (
                <>
                  <VStack spacing={2}>
                    <Box key={`details-key-${i}`}>
                      <Text color="white" fontWeight="500">
                        Name: {details.name}
                      </Text>
                      <Text color="white" fontWeight="500">
                        Birth year: {details.birth_year}
                      </Text>
                      <Text color="white" fontWeight="500">
                        Gender: {details.gender}
                      </Text>
                      <Text color="white" fontWeight="500">
                        Eye color: {details.eye_color}
                      </Text>
                      <Text color="white" fontWeight="500">
                        Hair color: {details.hair_color}
                      </Text>
                      <Text color="white" fontWeight="500">
                        Skin color: {details.skin_color}
                      </Text>
                      <Text color="white" fontWeight="500">
                        Height: {details.height}
                      </Text>
                      <Text color="white" fontWeight="500">
                        Mass: {details.mass}
                      </Text>
                      <Text color="white" fontWeight="500">
                        Films:{" "}
                      </Text>
                      {filmData.map((film: any, j: any) => {
                        return (
                          <li
                            style={{ listStyle: "none" }}
                            key={`film-id-${j}`}
                          >
                            {" "}
                            - {film.title}
                          </li>
                        );
                      })}
                      <HStack>
                        <Text color="white" fontWeight="500">
                          Race:{" "}
                        </Text>
                        {speciesData.map((race: any, k: any) => {
                          console.log(speciesData);
                          return <span key={`race-id-${k}`}>{race.name}</span>;
                        })}
                      </HStack>
                    </Box>
                  </VStack>
                </>
              );
            })}
          </Flex>
        </Flex>
      )}
    </>
  );
};

export default Profile;
